Corax Dashboard
===============

Prerequisites
-------------

Install ruby and bundler

Setup
-----

Checkout git repo

    git clone git@bitbucket.org:radiocorax/corax.dashboard.git
    cd corax.dashboard

Install ruby dependencies

    bundle install --deployment

Run Dashing (check Ruby version)

    bundle exec vendor/bundle/ruby/2.1.0/bin/dashing start

Access the dashboard at http://localhost:3030

Hacking
-------

The dashboard files can be found in the git checkouts root dir. Find detailed
guide at http://dashing.io/#setup
