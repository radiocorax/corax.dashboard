require 'json'
require 'rest-client'

SCHEDULER.every '20s' do
  playlist_day_json = RestClient.get 'http://192.168.2.10:9300/playlist/1/content.json'
  send_event('playlist_day', JSON.parse(playlist_day_json))

  playlist_night_json = RestClient.get 'http://192.168.2.10:9300/playlist/0/content.json'
  send_event('playlist_night', JSON.parse(playlist_night_json))
end
