max = 100
current = max

SCHEDULER.every '1s' do
  send_event('current',   { value: current-=1, max: max })
end
